#!/usr/bin/env gnuplot

##################################################################################################################
# SETTINGS
##################################################################################################################

# Filenames and variables
CNTRS_DATA = "../data/contours.dat"
WOODLAND_DATA = "../data/woodland-area.dat"
XMAX = 885
YMAX = 240

# Plot options
set terminal pdfcairo enhanced size 5.2,2.5 font "NimbusRomNo9L-Regu,22" fontscale 0.4
set xlabel "{/NimbusRomNo9L-ReguItal x} (m)" offset 0,0.75
set ylabel "{/NimbusRomNo9L-ReguItal y} (m)" offset 1,0
set xrange [0:XMAX]
set yrange [0:YMAX]
set xtics scale 0.5 add ("%g" XMAX) font ",18" offset 0,0.25
set ytics scale 0.5 add ("%g" YMAX) font ",18"
set style fill transparent pattern 5
set lmargin at screen 0.11
set bmargin at screen 0.17
set tmargin at screen 0.95
set macro

set output "../images/map-monit-points.pdf"

# Contour variables
xi = 0; xa = XMAX; yi = 0; ya = YMAX;
xl = xi + 0.075*(xa - xi); xh = xa - 0.075*(xa-xi);
yl = yi + 0.075*(ya - yi); yh = ya - 0.075*(ya-yi);
x0 = 0; y0 = 0; b = 0

# Contour functions
g(x,y)=(((x > xl && x < xh && y > yl && y < yh) ? (x0 = x, y0 = y) : 1), 1/0)
ws(x,y) = ( (x == x0 && y == y0) ? 1 : 1/0)
lab(x,y) = ( (x == x0 && y == y0) ? stringcolumn(3) : "")

# Contour macros
ZERO = "x0 = xi - (xa-xi), y0 = yi - (ya-yi), b = b+1"
SEARCH = "CNTRS_DATA index b u 1:(g($1,$2)) notitle"
PLOT = "CNTRS_DATA index b u 1:2 w lines lt 1 lw 1.5 lc rgb 'grey' notitle"
SPACE = "CNTRS_DATA index b u 1:2:(ws($1,$2)) w points pt 5 ps 1.5 lc rgb 'white' notitle"
LABEL = "CNTRS_DATA index b u 1:2:(lab($1,$2)) w labels tc rgb 'grey50' font ',16' notitle"


##################################################################################################################
# PLOT
##################################################################################################################

$LOCATIONS << EOD
	70 188 C1
	248 133 C2
	356 101 C3
	460 131 C4
	612 113 C5
	50 160 W1
	190 185 W2
	190 110 W3
	290 75 W4
	320 150 W5
	485 100 F1
	730 180 F2
	755 140 F3
EOD

set label 1 "Lopen Brook" at 515,150 tc rgb 'grey50' font ",15"
load "north-arrow.gnu"

plot	@SEARCH, @PLOT, @SPACE, @LABEL, @ZERO, \
		@SEARCH, @PLOT, @SPACE, @LABEL, @ZERO, \
		@SEARCH, @PLOT, @SPACE, @LABEL, @ZERO, \
		@SEARCH, @PLOT, @SPACE, @LABEL, @ZERO, \
		@SEARCH, @PLOT, @SPACE, @LABEL, @ZERO, \
		@SEARCH, @PLOT, @SPACE, @LABEL, @ZERO, \
		@SEARCH, @PLOT, @SPACE, @LABEL, \
		WOODLAND_DATA u 1:2 w filledcurves lc rgb 'grey50' notitle, \
		$LOCATIONS w labels font ",18" notitle
