#!/usr/bin/env gnuplot

##################################################################################################################
# SETTINGS
##################################################################################################################

# Filenames and variables
DATA = "../data/flood-hydrographs.dat"

# Plot options
set terminal pdfcairo monochrome enhanced dashed size 2.6,2.3 font "NimbusRomNo9L-Regu,22" fontscale 0.4
set xlabel "{/NimbusRomNo9L-ReguItal t} (hrs)" offset 0,0.5
set ylabel "{/NimbusRomNo9L-ReguItal Q} (m^3/s)" offset 1.5,0
set xrange [0:48]
set xtics 12 scale 0.5 font ",18"
set ytics 2 scale 0.5 font ",18"
set key Left reverse box lw 0.5 height 0.3 width 1 samplen 3 autotitle columnheader font ",18"

set output "../images/flood-hydrograph.pdf"


##################################################################################################################
# PLOT
##################################################################################################################

plot DATA index 4 u ($1/(60*60)):2 w lines
