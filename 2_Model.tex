%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Numerical Model}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Governing Equations}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Neglecting the barometric pressure and wind shear stress, the depth-averaged continuity and momentum components of the shallow water equations (SWEs) can be written in conservative form as:
%
\begin{subequations}
\label{eq:swe}
\begin{equation}
\label{eq:swe-continuity}
\phi \frac{\partial \eta}{\partial t} + \frac{\partial q_x}{\partial x} + \frac{\partial q_y}{\partial y} = 0
\end{equation}
\begin{align}
\begin{split}
\label{eq:swe-momentum-x}
\frac{\partial q_x}{\partial t} + \frac{\partial \left(\beta q_x^2 / H + g \eta^2 / 2 +gh\eta\right)}{\partial x}  + \frac{\partial \left(\beta q_x q_y / H\right)}{\partial y} = X + g \eta \frac{\partial h}{\partial x} \\- \phi \frac{g q_x \left(q_x^2 + q_y^2\right)^{1/2}}{H^2 C^2} + \nu_t \left(2\frac{\partial^2 q_x}{\partial x^2} + \frac{\partial^2 q_x}{\partial y^2} + \frac{\partial^2 q_y}{\partial x \partial y}\right)
\end{split}
\end{align}
\begin{align}
\begin{split}
\label{eq:swe-momentum-y}
\frac{\partial q_y}{\partial t} + \frac{\partial \left(\beta q_x q_y / H\right)}{\partial x}  + \frac{\partial \left(\beta q_y^2 / H + g \eta^2 / 2 +gh\eta\right)}{\partial y} = Y + g \eta \frac{\partial h}{\partial y} \\- \phi \frac{g q_y \left(q_x^2 + q_y^2\right)^{1/2}}{H^2 C^2} + \nu_t \left(\frac{\partial^2 q_y}{\partial x^2} + 2\frac{\partial^2 q_y}{\partial y^2} + \frac{\partial^2 q_x}{\partial x \partial y}\right)
\end{split}
\end{align}
\end{subequations}
%
%\begin{equation}
%\begin{split}
%\frac{\partial \mathbf{X}}{\partial t} + \frac{\partial \mathbf{F}}{\partial x} + \frac{\partial \mathbf{G}}{\partial y} = \mathbf{S} + \mathbf{T} \\
%\mathbf{X} = \begin{bmatrix}\phi \eta \\ q_x \\ q_y\end{bmatrix},\ \mathbf{F} = \begin{bmatrix}q_x \\ \frac{\beta q_x^2}{H} + \frac{g \eta^2}{2} +gh\eta \\ \frac{\beta q_x q_y}{H}\end{bmatrix},\ \mathbf{G} = \begin{bmatrix}q_y \\ \frac{\beta q_x q_y}{H} \\ \frac{\beta q_y^2}{H} + \frac{g \eta^2}{2} +gh\eta\end{bmatrix}, \\
%\mathbf{S} = \begin{bmatrix}0 \\ X + g \eta \frac{\partial h}{\partial x} - \phi \frac{g q_x \left(q_x^2 + q_y^2\right)^{1/2}}{H^2 C^2} + \nu_t \left(2\frac{\partial^2 q_x}{\partial x^2} + \frac{\partial^2 q_x}{\partial y^2} + \frac{\partial^2 q_y}{\partial x \partial y}\right) \\ 0\end{bmatrix}, \\
%\mathbf{T} = \begin{bmatrix}0 \\ 0 \\ Y + g \eta \frac{\partial h}{\partial y} - \phi \frac{g q_y \left(q_x^2 + q_y^2\right)^{1/2}}{H^2 C^2} + \nu_t \left(\frac{\partial^2 q_y}{\partial x^2} + 2\frac{\partial^2 q_y}{\partial y^2} + \frac{\partial^2 q_x}{\partial x \partial y}\right)\end{bmatrix}
%\end{split}
%\end{equation}
%
where $t$ is time; $\eta$ is the water surface elevation; $H~(= h + \eta)$ is the total water depth, with $h$ being the water depth below datum; $q_x$ and $q_y$ are the discharges per unit width in the $x$ and $y$ directions, respectively; $\beta$ is the correction factor for non-uniform vertical velocity profiles, where $\beta = 1.016$ for a seventh power-law velocity distribution; $g$ is the acceleration due to gravity; $X$ and $Y$ are the depth-averaged body forces, such as Coriolis or vegetative drag; $\phi$ is the solid volume fraction of the vegetation present within a computational cell; $C$ is the Chezy coefficient, calculated from the Manning formula in this study; and $\nu_t$ is the kinematic eddy viscosity.

The SWEs are solved here using an efficient finite difference TVD-MacCormack scheme, the full details of which can be found in \cite{Liang-06a}. The explicit cell-centered scheme is second-order accurate in both space and time \citep{Mingham-01}. The model uses the well-known MacCormack `predictor-corrector' scheme to solve the two one-dimensional hyperbolic equations gained from applying the operator-splitting technique to the SWEs. An additional TVD step, first proposed by \cite{Davis-84}, is then performed at the corrector stage so that spurious numerical oscillations are avoided in regions of sharp-gradients. This scheme has previously been successfully applied to model shallow water flows in converging channel \citep{Mingham-01}, dam-break \citep{Louaked-98, Liang-06a}, valley flooding \citep{Liang-07c}, and tidal estuarine \citep{Liang-10} scenarios.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Vegetative Drag Force}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Decoupling the bed friction and drag force, instead of artificially increasing the surface roughness, ensures that the bed shear stress remains physically correct. Therefore, the vegetation in the following case study is incorporated into the SWEs via a drag force term in the momentum equations (Eqs.~\ref{eq:swe-momentum-x} and \ref{eq:swe-momentum-y}) and a porosity term in the continuity equation (Eq.~\ref{eq:swe-continuity}).

In previous modelling studies riparian vegetation has typically been represented as rigid cylinders \citep{Stoesser-03, Gao-11, Zhang-13}. This is mainly due to the minimal input parameters required and the ease of computation. However, numerous experimental studies have found that this rigid approximation does not hold for natural vegetation \citep{Fathi-97, Oplatka-98, Jarvela-04a, Sand-Jensen-08a, Wilson-08, Schoneboom-11, Jalonen-13b, Siniscalchi-13, Whittaker-13}. Therefore, in this study, the drag force terms in the $x$ and $y$ directions, respectively, are determined using the model for flexible vegetation developed by \cite{Whittaker-14a}:
%
\begin{equation}
\left[F_x, F_y\right] = \frac{1}{2} N C_{d\chi} A_{p0} Ca^{\psi/2} \frac{\left[q_x, q_y\right] \left(q_x^2 + q_y^2\right)^{1/2}}{H^2}
\label{eq:veg-drag}
\end{equation}
%
where $N$ is the number of trees per m$^2$; $C_{d\chi}$ is a species-specific drag coefficient; $A_{p0}$ is the vegetation's projected area in still air; $Ca$ is the vegetative Cauchy number; and $\psi$ is a species-specific Vogel exponent.

The vegetative Cauchy number characterizes the compressibility of the vegetation in response to hydrodynamic loading and is determined as follows:
%
\begin{equation}
Ca = \frac{\rho \left(q_x^2 + q_y^2\right) A_{p0} H_v W}{H^2 EI}
\label{eq:Cauchy-number}
\end{equation}
%
where $H_v$ and $W$ are the height and maximum width of the submerged part of the vegetation, respectively; and $EI$ is the flexural rigidity of the main stem. In the case of $\psi < 0$, a limiter must be placed on the Cauchy number in order to prevent the combined $C_{d\chi} A_{p0}$ term from exceeding its initial value when there is no deformation, i.e. $Ca = \max(1, Ca)$.

Physically, Eqs.~\eqref{eq:veg-drag} and \eqref{eq:Cauchy-number} represent the magnitude ($Ca$) and rate ($\psi$) of the reconfiguration that flexible objects experience in fluid flows. It is also applicable to rigid bodies, since when $\psi = 0$ there will be no reduction in the combined $C_{d\chi} A_{p0}$ term with increasing flow velocity, assuming independence of Reynolds number effects.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Turbulence Closure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

A `zero-equation' eddy viscosity model, namely the depth-averaged parabolic model, is used here to calculate the kinematic eddy viscosity:
%
\begin{equation}
\nu_t = \frac{C_e \sqrt{g (q_x^2 + q_y^2)}}{C}
\label{eq:eddy-viscosity}
\end{equation}
%
where the empirical coefficient $C_e$ is set to a value of 1.2 \citep{Falconer-91}.

More advanced turbulence models, such as the `one-equation' Spalart-Allmaras or `two-equation' depth-averaged $k$-$\epsilon$ model proposed by \citet{Rastogi-78}, are not implemented in this study due to their higher computational cost. Furthermore, it has been shown that the velocity distributions predicted by the depth-averaged parabolic model are very similar to those predicted by the $k$-$\epsilon$ model and its variants for simple river channels \citep{Wu-04}. 